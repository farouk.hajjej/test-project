FROM openjdk:11
EXPOSE 8089
ADD target/tpAchatProject-1.0.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]