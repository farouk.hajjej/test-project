package com.esprit.examen.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

    @Bean(name = "swaggerApi1")
    public Docket swaggerApi1() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("group1")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.esprit.examen.group1"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    @Bean(name = "swaggerApi2")
    public Docket swaggerApi2() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("group2")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.esprit.examen.group2"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Swagger Configuration for DevOps Project")
                .description("\"Spring Boot Swagger configuration\"")
                .version("1.1.0")
                .build();
    }
}
